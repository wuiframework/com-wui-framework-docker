# com-wui-framework-docker v2019.0.1

> Docker environment focused on WUI Framework production and R&D needs.

## Requirements

This library does not have any special requirements, but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## History

### v2019.0.1
Added configuration for TeamCity and Docker registry.
### v2019.0.0
Added JIRA, Confluence, Wordpress and OwnCloud configuration. Added helper scripts.
### v2018.3.0
Initial release.

## License

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2018 [NXP](http://nxp.com/)
