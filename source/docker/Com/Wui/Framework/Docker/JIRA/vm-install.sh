#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright (c) 2019 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

sudo apt-get update
sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt-get update

apt-cache policy docker-ce
sudo apt-get -y install docker-ce
sudo apt-get install -y docker-compose

sudo ufw allow 80/tcp
sudo ufw allow 443/tcp
sudo ufw allow 22/tcp
sudo ufw allow 2222/tcp
sudo ufw enable

sudo chown -R bin:bin /var/atlassian/jira
sudo apt install openjdk-8-jdk
