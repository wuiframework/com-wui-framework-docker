#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright (c) 2018-2019 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

sudo docker run --name myadmin -d --link jira_mysql_1:mysql -p 2222:80 --net jira_default --env PMA_HOST=mysql phpmyadmin/phpmyadmin
