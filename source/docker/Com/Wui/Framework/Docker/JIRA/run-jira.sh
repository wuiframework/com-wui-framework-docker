#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright (c) 2018-2019 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

sudo docker run -itd --name jira --link jira_mysql:mysql -p 8080:8080 cptactionhank/atlassian-jira:latest
