#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright (c) 2018-2019 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

sudo docker run -itd --name jira_mysql -v /var/www/jiradb_data/:/var/lib/mysql -v /var/www/jiradb_backup/:/var/lib/backup \
--env MYSSQL_ROOT_PASSWORD=* --env MYSQL_DATABASE=jira --env MYSQL_USER=* --env MYSQL_PASSWORD=* -p 3306:3306 mysql:5.7
