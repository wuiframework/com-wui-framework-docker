#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright (c) 2018-2019 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
# sudo nano /etc/environment
# JAVA_HOME="/usr/lib/jvm/java-8-oracle"